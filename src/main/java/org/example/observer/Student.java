package org.example.observer;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class Student extends Observable {
    private final List<Float> notes;
    private float average;

    public Student() {
        notes = new ArrayList<Float>();
    }

    public void addNote(float note) {
        notes.add(note);
        this.setChanged();
        this.notifyObservers();
    }

    public List<Float> getNotes() {
        return notes;
    }

    public float getAverage() {
        return average;
    }

    public void setAverage(float average) {
        this.average = average;
    }
}

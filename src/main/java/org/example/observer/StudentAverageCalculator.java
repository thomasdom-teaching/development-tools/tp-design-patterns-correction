package org.example.observer;

import java.util.Observable;
import java.util.Observer;

public class StudentAverageCalculator implements Observer {
    @Override
    public void update(Observable studentObject, Object empty) {
        Student student = (Student) studentObject;
        System.out.println(studentObject);
        float average = student.getNotes().stream().reduce(0.00f, Float::sum) / student.getNotes().size();
        student.setAverage(average);
    }
}

package org.example.factory;

public class ShapeFactory {
    public Shape getShape(int numberOfSides, double sideLength) throws Exception {
        switch (numberOfSides) {
            case 1:
                return new Circle(sideLength / Math.PI / 2);
            case 3:
                return new EquilateralTriangle(sideLength);
            case 4:
                return new Square(sideLength);
            default:
                throw new Exception("Cannot create a polygon with these parameters!");
        }
    }
}

package org.example.factory;

public interface Shape {
    double getArea();
    double getPerimeter();
}

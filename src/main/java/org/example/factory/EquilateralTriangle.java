package org.example.factory;

public class EquilateralTriangle implements Shape {
    double sideLength;

    public EquilateralTriangle(double sideLength) {
        this.sideLength = sideLength;
    }

    @Override
    public double getArea() {
        return Math.sqrt(3) / 4 * Math.pow(sideLength, 2);
    }

    @Override
    public double getPerimeter() {
        return 3 * sideLength;
    }
}

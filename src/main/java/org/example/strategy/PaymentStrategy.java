package org.example.strategy;

public interface PaymentStrategy {
    double pay(double amount);
}

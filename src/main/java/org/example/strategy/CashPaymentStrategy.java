package org.example.strategy;

public class CashPaymentStrategy implements PaymentStrategy {
    @Override
    public double pay(double amount) {
        double serviceCharge = 5.00;
        return amount + serviceCharge;
    }
}

package org.example.strategy;

public class CreditCardPaymentStrategy implements PaymentStrategy {
    @Override
    public double pay(double amount) {
        double serviceCharge = 5.00;
        double paymentProcessingFee = 10.00;
        return amount + serviceCharge + paymentProcessingFee;
    }
}

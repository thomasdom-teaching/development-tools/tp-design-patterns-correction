package org.example.bridge;

import java.util.HashMap;
import java.util.Map;

public class Television implements Device {
    protected boolean isEnabled;
    protected int volume;
    protected Map<Integer, String> channels;
    protected int currentChannel;

    public Television() {
        isEnabled = false;
        volume = 50;
        this.channels = new HashMap<>();

        channels.put(1, "TF1");
        channels.put(2, "France 2");
        channels.put(3, "France 3");
        channels.put(4, "Canal +");
        channels.put(5, "France 5");
        channels.put(6, "M6");

        currentChannel = 1;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }

    @Override
    public void disable() {
        isEnabled = false;
    }

    @Override
    public void enable() {
        isEnabled = true;
    }

    @Override
    public void setVolume(int volume) {
        this.volume = volume;
    }

    @Override
    public int getVolume() {
        return volume;
    }

    @Override
    public void setChannel(int channel) {
        if (channels.containsKey(channel)) {
            currentChannel = channel;
        }
    }

    @Override
    public int getChannel() {
        return currentChannel;
    }

    public String getChannelName() {
        return channels.get(getChannel());
    }
}

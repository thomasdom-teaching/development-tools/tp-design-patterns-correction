package org.example.bridge;

public interface Device {
    boolean isEnabled();
    void disable();
    void enable();
    void setVolume(int volume);
    int getVolume();
    void setChannel(int channel);
    int getChannel();
}

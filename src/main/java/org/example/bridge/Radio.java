package org.example.bridge;

public class Radio implements Device {
    protected boolean isEnabled;
    protected int volume;
    protected int frequency;

    public Radio() {
        isEnabled = false;
        volume = 30;
        frequency = 100;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }

    @Override
    public void disable() {
        isEnabled = false;
    }

    @Override
    public void enable() {
        isEnabled = true;
    }

    @Override
    public void setVolume(int volume) {
        this.volume = volume;
    }

    @Override
    public int getVolume() {
        return volume;
    }

    @Override
    public void setChannel(int channel) {
        this.frequency = channel;
    }

    @Override
    public int getChannel() {
        return frequency;
    }
}

package org.example.observer;

import org.junit.Assert;
import org.junit.Test;

public class StudentAverageCalculatorTest {
    @Test
    public void should_return_the_correct_average_for_a_student() {
        Student student = new Student();
        StudentAverageCalculator calculator = new StudentAverageCalculator();
        student.addObserver(calculator);

        student.addNote(15.0f);
        Assert.assertEquals("Should have 15.00 of average", student.getAverage(), 15.00f, 0.00001);
        student.addNote(5.0f);
        Assert.assertEquals("Should have 10.00 of average", student.getAverage(), 10.00f, 0.00001);
        student.addNote(13.0f);
        Assert.assertEquals("Should have 11.00 of average", student.getAverage(), 11.00f, 0.00001);
    }
}

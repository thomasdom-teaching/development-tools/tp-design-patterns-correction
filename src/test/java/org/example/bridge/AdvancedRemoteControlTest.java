package org.example.bridge;

import org.junit.Assert;
import org.junit.Test;

public class AdvancedRemoteControlTest {
    @Test
    public void it_should_set_the_volume_up_for_a_television() {
        Device device = new Television();
        AdvancedRemoteControl remote = new AdvancedRemoteControl(device);
        remote.volumeUp();

        Assert.assertEquals(device.getVolume(), 60);
    }

    @Test
    public void it_should_set_the_volume_down_for_a_television() {
        Device device = new Television();
        AdvancedRemoteControl remote = new AdvancedRemoteControl(device);
        remote.volumeDown();

        Assert.assertEquals(device.getVolume(), 40);
    }

    @Test
    public void it_should_mute_the_volume_for_a_television() {
        Device device = new Television();
        AdvancedRemoteControl remote = new AdvancedRemoteControl(device);
        remote.mute();

        Assert.assertEquals(device.getVolume(), 0);
    }

    @Test
    public void it_should_set_the_channel_up_for_a_television() {
        Device device = new Television();
        AdvancedRemoteControl remote = new AdvancedRemoteControl(device);
        remote.channelUp();

        Assert.assertEquals(device.getChannel(), 2);
    }

    @Test
    public void it_should_set_the_channel_down_for_a_television() {
        Device device = new Television();
        device.setChannel(3);
        AdvancedRemoteControl remote = new AdvancedRemoteControl(device);
        remote.channelDown();

        Assert.assertEquals(device.getChannel(), 2);
    }

    @Test
    public void it_should_power_up_a_television() {
        Device device = new Television();
        AdvancedRemoteControl remote = new AdvancedRemoteControl(device);
        remote.togglePower();

        Assert.assertTrue(device.isEnabled());
    }

    @Test
    public void it_should_power_down_a_television() {
        Device device = new Television();
        device.enable();
        AdvancedRemoteControl remote = new AdvancedRemoteControl(device);
        remote.togglePower();

        Assert.assertFalse(device.isEnabled());
    }

    @Test
    public void it_should_set_the_volume_up_for_a_radio() {
        Device device = new Radio();
        AdvancedRemoteControl remote = new AdvancedRemoteControl(device);
        remote.volumeUp();

        Assert.assertEquals(device.getVolume(), 40);
    }

    @Test
    public void it_should_set_the_volume_down_for_a_radio() {
        Device device = new Radio();
        AdvancedRemoteControl remote = new AdvancedRemoteControl(device);
        remote.volumeDown();

        Assert.assertEquals(device.getVolume(), 20);
    }

    @Test
    public void it_should_mute_the_volume_for_a_radio() {
        Device device = new Radio();
        AdvancedRemoteControl remote = new AdvancedRemoteControl(device);
        remote.mute();

        Assert.assertEquals(device.getVolume(), 0);
    }

    @Test
    public void it_should_set_the_channel_up_for_a_radio() {
        Device device = new Radio();
        AdvancedRemoteControl remote = new AdvancedRemoteControl(device);
        remote.channelUp();

        Assert.assertEquals(device.getChannel(), 101);
    }

    @Test
    public void it_should_set_the_channel_down_for_a_radio() {
        Device device = new Radio();
        AdvancedRemoteControl remote = new AdvancedRemoteControl(device);
        remote.channelDown();

        Assert.assertEquals(device.getChannel(), 99);
    }

    @Test
    public void it_should_power_up_a_radio() {
        Device device = new Radio();
        AdvancedRemoteControl remote = new AdvancedRemoteControl(device);
        remote.togglePower();

        Assert.assertTrue(device.isEnabled());
    }

    @Test
    public void it_should_power_down_a_radio() {
        Device device = new Radio();
        device.enable();
        AdvancedRemoteControl remote = new AdvancedRemoteControl(device);
        remote.togglePower();

        Assert.assertFalse(device.isEnabled());
    }
}
